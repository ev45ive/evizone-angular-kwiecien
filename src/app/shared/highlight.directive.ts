import { Directive, ElementRef, Input, OnInit, OnDestroy, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]',
  // host:{
  //   '[style.color]':'getCurrentColor()',
  //   '(click)':'somethingOnClick($event.target)'
  // }
})
export class HighlightDirective implements OnInit, OnDestroy {

  // '[style.color]'
  @Input('appHighlight')
  highlightColor

  @HostBinding('style.color')
  get currentColor(){
    return this.active? this.highlightColor:'initial';
  } 

  active = false

  @HostListener('mouseenter',['$event.x'])
  activate(x:number){
    this.active = true
    // this.currentColor = this.highlightColor
  }

  @HostListener('mouseleave')
  deactivate(){
    this.active = false
    // this.currentColor = 'initial'
  }

  constructor(private elem: ElementRef) {
  }

  ngOnInit(){
    // console.log(
    //   this.elem.nativeElement, this.color
    // )

  }

  ngOnDestroy(){

  }

}

// console.log(HighlightDirective);
