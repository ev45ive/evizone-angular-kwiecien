import { Directive, TemplateRef, ViewContainerRef, Input, ViewRef } from '@angular/core';

var i = 0

@Directive({
  selector: '[appUnless]'
})
export class UnlessDirective {

  cache:ViewRef

  @Input()
  set appUnless(hide){
   
    if(hide){
      this.cache = this.vcr.detach()
    }else{
      if(this.cache){
        this.vcr.insert(this.cache)
      }else{
        this.vcr.createEmbeddedView(this.tpl,{
          $implicit: 'ala ma kota '+ ++i,
          // first, count, ala, kot, ...
        })
      }      
    }
  }

  @Input()
  set appUnlessAwesome(data){
    console.log('awesome',data)
  }


  constructor(
    private tpl:TemplateRef<any>,
    private vcr:ViewContainerRef
  ) { 
  
    
  }

}
