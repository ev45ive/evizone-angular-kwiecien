import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab',
  template: `
    <div class="card">
      <div class="card-body">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: []
})
export class TabComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
