export interface Named{
  name:string
}
export interface Identity{
  id:string;
}

// export interface Entity extends Named, Identity{
// }
export interface Entity{
  id:string
  name:string
}

export interface Album  extends Entity{
  images: AlbumImage[],
  artists: Artist[]
}

export interface Artist extends Entity{}

export interface AlbumImage{
  url:string
  width: number
  height:number
}
