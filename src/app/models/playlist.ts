export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  color: string;
  /**
   * Awesome Tracks list
   * nie pisz tego!
   */
  tracks?: Track[]
}

// tego nie piszcie:
interface Track {
  name: string;
}