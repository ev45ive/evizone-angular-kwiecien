import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http'

@Injectable()
export class AuthService {

  constructor() { }

  private token: string

  authorize() {
    const url = `https://accounts.spotify.com/authorize`

    const params = new HttpParams({
      fromObject: {
        client_id: 'e15c7d8bda684efeb48835a42a454c95',
        response_type: 'token',
        redirect_uri: 'http://localhost:4200/'
      }
    })

    sessionStorage.removeItem('token')
    window.location.href = (url + '?' + params.toString())
  }

  private token_param = 'access_token'

  getToken() {

    this.token = JSON.parse(sessionStorage.getItem('token'))

    if (!this.token) {
      const params = new HttpParams({
        fromString: window.location.hash.substr(1)
      })
      window.location.hash = ""

      this.token = params.get(this.token_param)
      sessionStorage.setItem('token', JSON.stringify(this.token))
    }

    if (!this.token) {
      this.authorize()
    }

    return this.token
  }

}
