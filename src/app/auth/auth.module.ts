import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './auth.service';
import { AuthInterceptorService } from './auth-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [

  ]
})
export class AuthModule {

  static forChild(){
    return {
      ngModule: AuthModule,
      providers:[
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptorService,
          multi: true
        },
      ]
    }
  }

  static forRoot(options): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService,
        // AuthInterceptorService
        // useExisting: AuthInterceptorService
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptorService,
          multi: true
        },
        {
          provide: 'auth_options',
          useValue: options
        }
      ]
    }
  }

  constructor(private auth: AuthService) {
    auth.getToken()
  }
}
