import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { empty } from 'rxjs/observable/empty';
import { _throw } from 'rxjs/observable/throw';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private auth: AuthService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(this.makeRequest(req)).pipe(
      catchError((err, cought) => {

        if (err instanceof HttpErrorResponse && err.status === 401) {
          this.auth.authorize()
          // auth.refreshToken().pipe( switchMap( () => next.handle(this.makeRequest(req)))

          return empty<HttpEvent<any>>()
        }

        return _throw(err)

        // return auth.refeshToken() > cought.pipe(delay(1250))
        // return _throw(new Error('Unauthorized'))
      })
    )
  }

  makeRequest(req) {
    return req.clone({
      setHeaders: {
        'Authorization': `Bearer ${this.auth.getToken()}`
      }
    })
  }

}


// getObser()
//   |> map()
//   |> filter()
//   |> catchError()
