import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { TabsComponent } from './tabs.component';
import { TabComponent } from './tab.component';
import { MusicModule } from './music/music.module';
import { AuthModule } from './auth/auth.module';
import { AppRoutingModule } from './app.routing';
import { TestingComponent } from './testing.component';

@NgModule({
  declarations: [
    AppComponent,
    TabsComponent,
    TabComponent,
    TestingComponent
  ],
  imports: [
    BrowserModule,
    PlaylistsModule,
    SharedModule,
    // MusicModule,
    AuthModule.forRoot({}),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [TabsComponent, TabComponent]
})
export class AppModule { 

  ngDoBootstrap(){

  }

}
