import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistsComponent } from './playlists.component';
import { PlaylistContainerComponent } from './playlist-container.component';
import { ListContainerComponent } from './list-container.component';
import { PlaylistResolverService } from './playlist-resolver.service';

const routes: Routes = [
  {
    path: 'playlists',
    component: PlaylistsComponent,
    children:[
      {
        path:'',
        component: ListContainerComponent,
        outlet:'list'
      },
      {
        path:'',
        component: PlaylistContainerComponent
      }
    ]
  },
  {
    path: 'playlists/:id',
    component: PlaylistsComponent,
    data:{
      option: true
    },
    resolve:{
      playlist: PlaylistResolverService
    },
    children:[
      {
        path:'',
        component: ListContainerComponent,
        outlet:'list'
      },
      {
        path:'',
        component: PlaylistContainerComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule { }
