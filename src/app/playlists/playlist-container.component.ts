import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlaylistsService } from './playlists.service';
import { share, map, switchMap, tap, pluck } from 'rxjs/operators';
import { Playlist } from '../models/playlist';

@Component({
  selector: 'app-playlist-container',
  template: `
    <app-playlist-details *ngIf="selected | async as playlist"
      (save)="save($event)"
      [playlist]="playlist">
    </app-playlist-details>

    <div *ngIf="!(selected | async)">Please select playlist</div>
  `,
  styles: []
})
export class PlaylistContainerComponent implements OnInit {

  selected

  constructor(
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService
  ) {
    
    this.selected = route.data.pipe(
      pluck<{},Playlist>('playlist')
    )

    // this.selected = this.route.paramMap.pipe(
    //   map(params => params.get('id')),
    //   map(id => parseInt(id)),
    //   // map(parseInt.bind(null,10)),
    //   // tap(console.log),
    //   switchMap(id => this.playlistsService.getPlaylist(id))
    // )

  }

  save(playlist: Playlist) {
    this.playlistsService.savePlaylist(playlist)
  }


  ngOnInit() {
  }

}
