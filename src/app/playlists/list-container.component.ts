import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlaylistsService } from './playlists.service';
import { map, filter, switchMap, tap, share } from 'rxjs/operators';

@Component({
  selector: 'app-list-container',
  template: `
    <app-items-list 
      [items]="playlists | async"
      [selected]="selected | async">
    </app-items-list>
  `,
  styles: []
})
export class ListContainerComponent implements OnInit {

  selected

  playlists = this.playlistsService.getPlaylists()

  constructor(
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService
  ) {

    console.log(this.route.snapshot.data)
    
    this.selected = this.route.paramMap.pipe(
      map(params => params.get('id')),
      filter(id => !!id),
      // tap(console.log),
      switchMap(id => this.playlistsService.getPlaylist(parseInt(id)))
    )
  }

  ngOnInit() {

  }
}
