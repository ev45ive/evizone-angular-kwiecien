import { Injectable } from '@angular/core';
import { of } from 'rxjs/observable/of';
import { Playlist } from '../models/playlist';
import { delay } from 'rxjs/operators';

@Injectable()
export class PlaylistsService {

  playlists: Playlist[] = [
    {
      id: 123,
      name: 'Angular Hits',
      favourite: false,
      color: '#ff0000'
    },
    {
      id: 234,
      name: 'Best of Angular',
      favourite: true,
      color: '#00ff00'
    },
    {
      id: 345,
      name: 'Angular TOP 20!',
      favourite: false,
      color: '#0000ff'
    },
  ]

  constructor() { }

  getPlaylists() {
    return of(this.playlists)
  }

  getPlaylist(id: number) {
    return of(this.playlists.find(
      playlist => playlist.id == id
    )).pipe(
      // delay(2500)
    )
  }

  savePlaylist(playlist:Playlist){
    const index = this.playlists.findIndex(
      old => old.id === playlist.id
    )
    if (index !== -1){
      this.playlists.splice(index,1,playlist)
      // this.selected = playlist
    }
  }

}
