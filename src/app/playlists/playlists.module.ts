import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistsComponent } from './playlists.component';
import { ItemsListComponent } from './items-list.component';
import { ListItemComponent } from './list-item.component';
import { PlaylistDetailsComponent } from './playlist-details.component';

import { FormsModule } from '@angular/forms'
import { SharedModule } from '../shared/shared.module';
import { PlaylistsService } from './playlists.service';
import { PlaylistsRoutingModule } from './playlists-routing.module';
import { PlaylistContainerComponent } from './playlist-container.component';
import { ListContainerComponent } from './list-container.component';
import { PlaylistResolverService } from './playlist-resolver.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    PlaylistsRoutingModule
  ],
  declarations: [
    PlaylistsComponent,
    ItemsListComponent,
    PlaylistDetailsComponent,
    ListItemComponent,
    PlaylistContainerComponent,
    ListContainerComponent,
  ],
  exports: [
    PlaylistsComponent
  ],
  providers: [
    PlaylistsService,
    PlaylistResolverService
  ]
})
export class PlaylistsModule { }
