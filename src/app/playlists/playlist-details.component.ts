import { Component, OnInit, Attribute, Input, Output, EventEmitter } from '@angular/core';
import { Playlist } from '../models/playlist';

@Component({
  selector: 'app-playlist-details',
  template: `
    <ng-container [ngSwitch]="mode">
      <div *ngSwitchDefault>
        <dl class="row">
          <dt class="col-3">Name</dt>
          <dd class="col-9">{{playlist.name}}</dd>

          <dt class="col-3">Favourite</dt>
          <dd class="col-9">{{playlist.favourite? 'Yes' : 'Nope'}}</dd>

          <dt class="col-3">Color</dt>
          <dd class="col-9" [style.color]="playlist.color">{{playlist.color}}</dd>
        </dl>
        <button class="btn btn-info" (click)="edit()">Edit</button>
      </div>

      <form *ngSwitchCase=" 'editor' " (submit)="save(formRef)"  #formRef="ngForm">
        <div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control"
            [ngModel]="playlist.name" name="name">
        </div>
        <div class="form-group">
          <label>Favourite</label>
          <input type="checkbox" 
            [ngModel]="playlist.favourite" name="favourite">
        </div>
        <div class="form-group">
          <label>Color</label>
          <input type="color" 
            [(ngModel)]="playlist.color" name="color">
        </div>
        <input type="button" value="Cancel" class="btn btn-danger" (click)="cancel()">
        <input type="submit" value="Save" class="btn btn-success">
      </form>
    </ng-container>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  @Input()
  playlist: Playlist

  draft: Playlist

  mode = 'details'

  constructor(@Attribute('placki') placki) { }

  @Output('save')
  saveEmitter = new EventEmitter<Playlist>()

  save(form) {
    this.saveEmitter.emit({
      ...this.playlist,
      ...form.value
    })
    this.mode = 'details'
  }

  edit() {
    this.mode = 'editor'
  }

  cancel() {
    this.mode = 'details'
  }


  ngOnInit() {
  }

}
