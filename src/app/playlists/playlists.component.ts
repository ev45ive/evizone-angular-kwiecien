import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-playlists',
  template: `
    <div class="row">
      <div class="col">
        <router-outlet name="list"></router-outlet>
      </div>
      <div class="col">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistsComponent implements OnInit {

  ngOnInit() {
  
  }

}
