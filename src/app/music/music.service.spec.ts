import { TestBed, inject } from '@angular/core/testing';

import { HttpClientTestingModule, HttpTestingController, RequestMatch } from '@angular/common/http/testing'

import { MusicService, SEARCH_URL } from './music.service';
import { HttpRequest } from '@angular/common/http';

fdescribe('MusicService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: SEARCH_URL,
          useValue: 'FAKE_HOST'
        },
        MusicService
      ]
    });
  });

  it('should be created', inject([MusicService], (service: MusicService) => {
    expect(service).toBeTruthy();
  }));

  it('should request search api',
    inject([MusicService, HttpTestingController], (service: MusicService, ctrl: HttpTestingController) => {

      service.getAlbums().subscribe(albums => {
        console.log(albums);
        
      })

      service.search('test')
      ctrl.expectOne('FAKE_HOST?type=album&q=test').flush({
        albums:{
          items:[
            {
              name:'Test'
            }
          ]
        }
      })
      ctrl.verify()
    }))
});
