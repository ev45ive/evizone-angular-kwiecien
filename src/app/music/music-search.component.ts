import { Component, OnInit, Inject } from '@angular/core';
import { Album } from '../models/album';
import { MusicService } from './music.service';
import { Subscription } from 'rxjs/Subscription';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-music-search',
  template: `
    <div class="row">
      <div class="col">
        <app-search-form 
            (queryChange)="search($event)">
        </app-search-form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        {{message}}
        <app-albums-list [albums]="albums$ | async ">
        </app-albums-list>
      </div>
    </div>
  `,
  styles: []
})
export class MusicSearchComponent implements OnInit {

  albums$ = this.music.getAlbums().pipe(
    tap((albums) => {
      this.message = "Found " + albums.length
    })
  )

  constructor(private music: MusicService) { }

  message = ''

  search(query) {
    // console.log(query);
    
    this.message = "Searching ..."
    this.music.search(query)
  }

  ngOnInit() {
  }

}
