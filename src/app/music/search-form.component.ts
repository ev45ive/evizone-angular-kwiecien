import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { filter, distinctUntilChanged, debounceTime, debounce, map, delay, combineLatest, withLatestFrom, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { FormControl, FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors, AsyncValidatorFn } from '@angular/forms';
import { Observer } from 'rxjs/Observer';
import { of } from 'rxjs/observable/of';

@Component({
  selector: 'app-search-form',
  template: `   
    <div class="form-group mb-3" [formGroup]="queryForm">
      <input type="text" class="form-control"
      placeholder="Search albums ..." formControlName="query">
      
      <div *ngIf="queryForm.pending">Please wait...</div>

      <app-form-feedback [field]="queryForm.get('query')">

        <div *ngIf="queryForm.get('query').getError('censor') as error">
          Cannot contain words "{{error.matched}}"
        </div>

      </app-form-feedback>
      
      <!-- <app-typical-form-field [field]="queryForm.get('query')" placeholder="search">
      </app-typical-form-field> -->
    </div>
  `,
  styles: [`
    .form-group .ng-invalid.ng-touched,
    .form-group .ng-invalid.ng-dirty {
      border: 2px solid red !important;
    }
  `]
})
export class SearchFormComponent implements OnInit {

  queryForm: FormGroup

  @Output()
  queryChange: Observable<string>

  constructor(private fb: FormBuilder) {

    this.queryForm = this.fb.group({
      'query': this.fb.control('',
        [
          Validators.required,
          Validators.minLength(3),
          this.censor('batman'),
        ],
        [
          this.asyncCensor('superman'),
        ]
      ),
    })

    const queryField = this.queryForm.get('query')

    this.queryChange = this.validValueChange(queryField)
  }

  validValueChange(field: AbstractControl) {

    const status$ = field.statusChanges
    const value$ = field.valueChanges

    return status$.pipe(
      debounceTime(400),
      filter<string>(status => status === "VALID"),
      withLatestFrom(value$, (valid, value) => value)
    )
  }

  asyncCensor(badword): AsyncValidatorFn {

    return (control: AbstractControl) => {

      const fakeResponse = this.censor(badword)

      return Observable.create((observer: Observer<ValidationErrors | null>) => {
        const errors = fakeResponse(control)

        const handler = setTimeout(() => {
          observer.next(errors)
          observer.complete()
        }, 1000)

        return function unsubscribe() {
          clearTimeout(handler)
        }
      })

      // return of(true).pipe(
      //   map(()=>fakeResponse(control.value)),
      //   delay(2000),
      // )

      // return this.http.get('/validation').pipe(map(response => validationErrors ))
    }
  }

  // censor: ValidatorFn = () => {
  //    return null
  // }

  censor(badword): ValidatorFn {

    return (control: FormControl): ValidationErrors | null => {

      const hasError = (control.value || '').includes(badword)

      if (hasError) {
        // <div> {{ form.get('field').getError('censor').badword }} </div>
        return {
          'censor': { matched: badword }
        }
      } else {
        return null
      }

    }
  }

  ngOnInit() {
  }

}
