import { RouterModule, Routes } from '@angular/router'
import { NgModule } from '@angular/core';
import { MusicSearchComponent } from './music/music-search.component';

const routes: Routes = [
  {
    path: 'music',
    loadChildren: 'app/music/music.module#MusicModule'
  },
  {
    path: '',
    redirectTo: 'playlists',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'music'
  },
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // enableTracing: true,
      // useHash: true,
      // errorHandler:()=>{}
      // onSameUrlNavigation:'reload',
      // paramsInheritanceStrategy:'emptyOnly',
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

// ng new wasza_apka --routing true
// ng g m moj_modul --routing true