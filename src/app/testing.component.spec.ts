import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { TestingComponent, TestingServiceToken } from './testing.component';
import { By } from '@angular/platform-browser';
import { FormsModule, NgModel } from '@angular/forms';

class MockService{
  message = 'placki'
  getMessage(){
    return this.message
  }
}

describe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;

  beforeEach( async(() => {

    TestBed.configureTestingModule({
      declarations:[
        TestingComponent
      ],
      imports:[
        FormsModule
      ],
      providers:[
        {
          provide: TestingServiceToken,
          useClass: MockService
        }

      ]
    }).compileComponents()
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create with service', inject([TestingServiceToken],(testingservice) => {
    expect(component).toBeTruthy();
    expect(testingservice.getMessage()).toEqual('placki')
  });

  it('should render text', () => {
    const p = fixture.debugElement.query(By.css('p'));
    const text = (p.nativeElement as HTMLElement).innerText

    expect(text).toMatch('testing')
  });

  it('should show changed text', () => {
    const p = fixture.debugElement.query(By.css('p'));
    component.message = "Changed!"
    
    fixture.detectChanges()
    const text = (p.nativeElement as HTMLElement).innerText
    expect(text).toMatch('Changed!')
  });

  it('should show message in input', () => {
    const input = fixture.debugElement
                  .query(By.directive(NgModel));
    
    expect(input).toBeTruthy('Input not found') ;
    
    return fixture.whenRenderingDone()
    .then(()=>{
      expect(input.properties.value)
                  .toEqual(component.message)
    })
  })


  it('should show message in input', () => {
    const input = fixture.debugElement
                  .query(By.directive(NgModel));

    updateInput(input,'Test').then(()=>{
        expect(component.message).toEqual('Test')
    })
  })

  function updateInput(input,value, event = 'input'){
    input.nativeElement.value = value
    input.triggerEventHandler(event,{
      target: input.nativeElement
    })
    return fixture.whenRenderingDone()
  }

});

